package ru.tsc.almukhametov.tm;

import static org.junit.Assert.assertTrue;
import static ru.tsc.almukhametov.tm.util.WorldUnit.convertBytes;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class SalutationTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    public static void main(final String[] args) {
        final long[] l = new long[]{1l, 4343l, 43434334l, 3563543743l};
        for (final long ll : l) {
            System.out.println(convertBytes(ll));
        }
    }
}

