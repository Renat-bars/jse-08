package ru.tsc.almukhametov.tm;

import ru.tsc.almukhametov.tm.api.ICommandRep;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.model.Command;
import ru.tsc.almukhametov.tm.repository.CommandRep;
import ru.tsc.almukhametov.tm.util.WorldUnit;

import java.util.Scanner;

public class Salutation {

    private static final ICommandRep COMMAND_REP = new CommandRep();

    public static void main(final String[] args) {
        System.out.println("** Welcome to THE REAL WORLD **");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public static void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.INFO:
                showInfo();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                showErrCommand();
        }
    }

    private static void showErrCommand() {
        System.err.println("Error, command not found");
    }

    public static void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            default:
                showErrArg();
        }
    }

    private static void showErrArg() {
        System.err.println("Error, argument not supported");
        System.exit(1);
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + WorldUnit.convertBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = WorldUnit.convertBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + WorldUnit.convertBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + WorldUnit.convertBytes(usedMemory));
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Renat Almukhametov");
        System.out.println("E-MAIL: rralmukhametov@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.1");
    }

    public static void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = COMMAND_REP.getTerminalCommands();
        for (final Command command : commands) showCommandValue(command.getName());
    }

    public static void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = COMMAND_REP.getTerminalCommands();
        for (final Command command : commands) showCommandValue(command.getArgument());
    }

    private static void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REP.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);

    }
}
